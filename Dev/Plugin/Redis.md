### Redis

---

5种类型

1. String

   常规K/V缓存应用，一个键最大存储512M

2. Hash

   String类型的field（属性）和value的映射表，适用于存储对象（多属性）[hget、hset、hgetall等]

3. List

   字符串列表，依插入顺序排序（LinkedList）双向[lpush（左新增）、rpush（右新增）、lpop（左移除）、rpop（右移除），lrange（列表片段）等]

4. Set

   自动排重，判断元素是否存在，集合操作

5. zSet

   依照某个条件排序，排行榜效果[zadd、zrange、zrem、zcard等]

6. Pub/Sub

   消息订阅，当K值进行消息发布后所有订阅的客户端都会收到对应的消息，即时聊天，群聊等