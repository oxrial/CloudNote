# Springboot Security

---

### 1.RBAC(Role-Base Access Control:基于角色的访问控制)

用户（user）U

角色（身份，role）R

权限（permission）P

U manytomany R 多对多

R manytomany P 多对多

例：

公司中的用户 U：张三

担任的角色R：经理，CEO等  U->R

经理角色可以是多个用户：R->U

R->P经理有多个权限：管理财务。。。

P->R，不止经理可以管理财务还有其他角色

