```java
package com.etc.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

/*
* 切面类的具体实现，又叫通知或加强
 */
public class TimeAspect {

    private static Logger log = Logger.getLogger(TimeAspect.class);

    //环绕方法执行
    //环绕方法的参数一定是：ProceedingJoinPoint 类型
    public Object around(ProceedingJoinPoint pjp) throws Throwable{
        log.debug("方法开始执行时的时间：" + System.currentTimeMillis());
        Object res = pjp.proceed();//执行目标业务方法
        log.debug("方法执行之后的时间：" + System.currentTimeMillis());
        return res;
    }
}
```

