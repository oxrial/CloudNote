### AOP配置 

<h6 style="text-align:right">java内容见 File/TimeAspect.java</h6>

---
```xml
<!--启动mvc的注解驱动-->
<mvc:annotation-driven></mvc:annotation-driven>
<context:component-scan base-package="com.etc"></context:component-scan>
<!--计时的切面具体实现即通知-->
<bean id="timeAspect" class"com.xx.aop.TimeAspect"></bean>
<!--aop配置-->
<aop:config>
	<!--切面配置-->
    <aop:aspect ref="timeAspect">
    	<!--切入点配置-->
        <aop:pointcut id="pointCut" expression="execution(* com.xx.service.*.*(..))">   
        </aop:pointcut>
        <!--环绕-->
        <aop:around></aop:around>
    </aop:aspect>
</aop:config>
```

