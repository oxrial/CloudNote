```java
package com.etc.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

/*
* 切面类的具体实现，又叫通知或加强
 */
public class TimeAspect {

    private static Logger log = Logger.getLogger(TimeAspect.class);

    //方法执行之前
    public void beforeMethod(){
        log.debug("方法开始执行时的时间：" + System.currentTimeMillis());
    }

    //方法执行之后
    public void afterMethod(){
        log.debug("方法执行之后的时间：" + System.currentTimeMillis());
    }
}
```

