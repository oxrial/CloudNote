```java
package com.etc.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/*
* 切面类的具体实现，又叫通知或加强
 */
@Component
@Aspect
public class TimeAspect {

    private static Logger log = Logger.getLogger(TimeAspect2.class);

    @Pointcut("execution(* com.etc.service.*.*(..))")
    public void pointCut(){}
    //方法执行之前
    @Before("pointCut()")
    public void beforeMethod(){
        log.debug("方法开始执行时的时间：" + System.currentTimeMillis());
    }

    //方法执行之后
    @After("pointCut()")
    public void afterMethod(){
        log.debug("方法执行之后的时间：" + System.currentTimeMillis());
    }

}

```