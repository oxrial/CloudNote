### AOP配置 

<h6 style="text-align:right">java内容见 File/TimeAspect.java</h6>

---

```XML
<!--启动mvc的注解驱动-->
<context:component-scan base-package="com.etc"></context:component-scan>
<!--开启注解代理功能，实现aop的操作-->
<aop:aspectj-autoproxy></aop:aspectj-autoproxy>
```

