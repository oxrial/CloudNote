### SPRINGCLOULD

---

例购物订单支付，创建订单-订单支付订单状态-库存-仓储-积分  [springclould底层](https://www.cnblogs.com/jajian/p/9973555.html)

![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162515.png)

##### Eureka

![image-20210302162633271](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162633.png)

注册中心，服务的注册和发现，eureka server中注册了所有的服务，当请求时在eureka中寻找对应的请求地址即可正确的访问到对应的服务。

##### Feign

服务简化代码

```
1.首先，如果你对某个接口定义了@FeignClient注解，Feign就会针对这个接口创建一个动态代理。
2.接着你要是调用那个接口，本质就是会调用 Feign创建的动态代理，这是核心中的核心。
3.Feign的动态代理会根据你在接口上的@RequestMapping等注解，来动态构造出你要请求的服务的地址。
4.最后针对这个地址，发起请求、解析响应
```

![image-20210302162716672](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162716.png)

##### Rebbon

负载均衡，若服务于不同的地址上，Feign无法获取地址，Rebbon则分发请求。

![image-20210302162839507](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162839.png)

```
1.首先Ribbon会从 Eureka Client里获取到对应的服务注册表，也就知道了所有的服务都部署在了哪些机器上，在监听哪些端口号。
2.然后Ribbon就可以使用默认的Round Robin算法，从中选择一台机器，若请求10个，有abcde台服务，则按顺序abcdeabcde。
3.Feign就会针对这台机器，构造并发起请求。
```

##### Hystrix

隔离熔断降级（线程池），将每个服务都分成几个独立的线程池，若一个线程池中服务异常其他线程池中的服务不受影响。提供熔断措施记录

![image-20210302162957413](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162957.png)

##### Zuul

微服务网关，统一的降级、限流、认证授权、安全

```
Eureka：各个服务启动时，Eureka Client都会将服务注册到Eureka Server，并且Eureka Client还可以反过来从Eureka Server拉取注册表，从而知道其他服务在哪里
Ribbon：服务间发起请求的时候，基于Ribbon做负载均衡，从一个服务的多台机器中选择一台
Feign：基于Feign的动态代理机制，根据注解和选择的机器，拼接请求URL地址，发起请求
Hystrix：发起请求是通过Hystrix的线程池来走的，不同的服务走不同的线程池，实现了不同服务调用的隔离，避免了服务雪崩的问题
Zuul：如果前端、移动端要调用后端系统，统一从Zuul网关进入，由Zuul网关转发请求给对应的服务
```

![image-20210302163030671](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302163030.png)
