Job for network.service failed because the control process exited with error code. See "systemctl status network.service" and "journalctl -xe" for details.

检查网络

ifconfig

若发现virbr0则禁用

```
ifconfig virbr0 down
systemctl disable libvirtd
```

若问题依旧则停用 NetworkManager.service

```
systemctl status NetworkManager.service
systemctl stop NetworkManager.service
禁用（可选）
systemctl disable NetworkManager.service
```

MAC地址不一致需变更

```
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="static"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33"
UUID="6b0b1f2f-5c64-46c8-b590-858174f161df"
DEVICE="ens33"
ONBOOT="yes"
IPADDR="192.168.19.139"
GATEWAY="192.168.19.1"
NETMASK=255.255.255.0"
HWADDR="00:50:56:2B:CF:93"
```

重启网卡

```
systemctl start network.service
```

