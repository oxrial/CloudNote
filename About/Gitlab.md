#### 本地项目推送至新建仓库

在网页新建仓库

linux中进入需上传的项目中

```
git init
若提示Reinitialized existing Git repository in /app/v8/ap-parent/.git/
git add *
添加到缓存区
git commit -m "注释"
git remote add origin http://192.168.19.138/root/v8-ap.git
若提示fatal: remote origin already exists.
git remote rm origin
提交到远程仓库
git remote add origin http://192.168.19.138/root/v8-ap.git
推送
git push -u origin master

```

#### CI/CD

CI：持续集成：在代码构建过程中持续地进行代码的集成、构建、以及自动化测试等，在代码提交的过程中通过单元测试等尽早地发现引入的错误。

CD：持续交付：在代码构建完毕后，可以方便地将新版本部署上线，快速迭代并交付产品。

#### JOB

job：任务：GitLab CI 系统中可以独立控制并运行的最小单位，针对特定的 commit
完成一个或多个 job。

#### PIPELINE

pipeline：流水线：像流水线一样执行多个 Job. 在代码提交或 MR 被合并时，GitLab 可以在最新生成的 commit
上建立一个 pipeline，在同一个 pipeline 上产生的多个任务中，所用到的代码版本是一致的。

#### STAGE

stage：阶段：在流水线中划分为多个阶段，只有当前一阶段的所有任务都执行成功后，下一阶段的任务才可被执行。如果某一阶段的任务均被设定为“允许失败”，那这个阶段的任务执行情况，不会影响到下一阶段的执行。