# V8 系统部署

1. #### 系统检查
   
1. ##### selinux
   
      ```
      	getenforce
      若打印结果为Enforcing则
      	vi /etc/selinux/config
   ```
   
2. ##### 关闭防火墙
   
      ```
      	systemctl status firewalld查看状态
      若防火墙的Active为active(running)则执行
      	systemctl stop firewalld停止防火墙
      	systemctl disable firewalld永久关闭防火墙
      注：iptables firewall：service iptables status/stop/off
      ```
   
   3. ##### 旧版本Mysql及Mariadb包检查并卸载
   
      ```
      npm -qa|grep Mysql/mysql/mariadb/Mariadb
      rpm -ev bao名
      ```
   4. ##### yum本地源挂载
   
      ```
      编辑vi /etc/fstab 后执行mount -a挂载fstab文件中的所有文件系统
      	在末尾段新增：/dev/sr0 /mnt iso9660 defaults 0 0
      	vi /etc/yum.repos.d/rhel-source.repo
          [iso]
          name=rhel-source  #自定义名称
          baseurl=file:///mnt   #本地光盘挂载路径
          enabled=1                   #启用yum源，0为不启用，1为启用
          gpgcheck=0                  #检查GPG-KEY，0为不检查，1为检查   #GPG-KEY路径
      配置完成后
          yum makecache  #缓存本地yum源中的软件包信息
      	yum repolist  #查看yum是否可用
      ```
   
1. #### vdfpd

   ```
   yum install vsftpd -y
   查看vsftpd服务状态
   
   service vsftpd status 
   
   开启vsftpd服务
   service vsftpd start 
   
   设置vsftpd开机自启动
   chkconfig vsftpd on
   
   修改vsftpd配置
   vi /etc/vsftpd/ftpusers    
   vi /etc/vsftpd/user_list
   将root用户去掉
   
   打开被动模式
   [root@v6apps1 ~]# vi /etc/vsftpd/vsftpd.conf
   添加以下配置打开被动模式
   pasv_enable=YES  #允许被动模式并设置端口范围
   pasv_min_port=24500  #被动模式端口号段最小端口号
   pasv_max_port=24600  #被动模式端口号段最大端口号
   pasv_promiscuous=YES  #这个是用于检测pasv的安全检查，YES为关闭安全检查 
   ```

1. #### ftp、setroubleshoot、unzip、gcc

   ```
   yum install ftp/setroubleshoot/unzip/gcc
   ```

1. #### Mysql

   ```
   rpm -qa | grep mysql
   find / -name mysql
   根据需求情况使用命令 rm -rf  xxx 依次删除以上文件，
   如：rm -rf /etc/selinux/targeted/active/modules/100/mysql；
   rpm -qa | grep mariadb
   rpm -e -nodeps mariadb-libs-5.5.52-1.el7.x86_64
   rm /etc/my.cnf
   1、在/usr/local下解压并重命名安装包
   tar -zxvf mysql-8.0.17-el7-x86_64.tar.gz
   mv mysql-8.0.17-el7-x86_64 mysql
   2、创建mysql组和用户
   # groupadd mysql
   # useradd -g mysql mysql
   # passwd mysql
   创建数据库data目录并赋予777权限
   # cd /usr/local/mysql
   # mkdir data
   # chmod -R 777 /usr/local/mysql/data
   3、Mysql目录所属组调整
   # chown -R mysql:mysql /usr/local/mysql
   4、新建mysql配置文件 my.cnf
   [mysql]
   # 设置mysql客户端默认字符集
   default-character-set=UTF8MB4
   
   [mysqld]
   skip-name-resolve
   #设置3306端口
   port = 3306
   # 设置mysql的安装目录
   basedir=/usr/local/mysql
   # 设置mysql数据库的数据的存放目录
   default-authentication-plugin=mysql_native_password
   datadir=/usr/local/mysql/data
   # 允许最大连接数
   max_connections=200
   # 服务端使用的字符集默认为8比特编码的latin1字符集
   character-set-server=UTF8MB4
   # 创建新表时将使用的默认存储引擎
   default-storage-engine=INNODB
   lower_case_table_names=1
   max_allowed_packet=16M
   
   5、	安装mysql并记录随机生成的密码
   # cd /usr/local/mysql/bin/
   # ./mysqld --initialize --console --lower_case_table_names=1
   
   6、	创建软连接(实现可直接命令行执行mysql)
   # ln -s /usr/local/mysql/bin/mysql /usr/bin
   7、	mysqld配置,拷贝启动文件到/etc/init.d/下并重命名为mysqld
   增加执行权限、检查自启动项列表中没有mysqld、如果没有就添加mysqld、设置开机启动
   # cp /usr/local/mysql/support-files/mysql.server  /etc/init.d/mysqld
   # chmod 777 /etc/init.d/mysqld
   # chkconfig --list mysqld
   # chkconfig --add mysqld
   # chkconfig mysqld on
   chown -R mysql:mysql /usr/local/mysql
   切换至mysql用户
   service mysqld start
   # mysql -u root -p
   输入随机生成的密码进入
   # set PASSWORD = 'root';
   # use mysql
   # UPDATE user SET `Host` = '%' WHERE `User` = 'root' LIMIT 1;
   # flush privileges;
   # SELECT `Host`,`User` FROM user;
   
   ```
   
1. #### jdk

   ```
   # mkdir -p /usr/java
   将jdk-8u161-linux-x64.tar.gz上传到java目录下并解压
     # vi /etc/profile
   export JAVA_HOME=/usr/java/jdk1.8.0_161   #jdk的解压路径
   export PATH=$PATH:$JAVA_HOME/bin
   export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
   java -version (查看安装的版本)#执行此步前到/bin根目录   删除java文件
   ```

1. #### maven

   ```
   上传maven压缩包解压
   tar -xzvf apache-maven-3.5.2-bin.tar.gz，注意用户权限，为root用户。
     vi /etc/profile #修改配置文件
     export JAVA_HOME=/usr/java/jdk1.8.0_45
   export SVN_HOME=/home/svn/subversion
   export MAVEN_HOME=/app/nexus/apache-maven-3.0.4
   export PATH=$PATH:$HOME/bin:$JAVA_HOME/bin:$SVN_HOME/bin:$MAVEN_HOME/bin
   export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
     source /etc/profile  #使配置文件生效
   mvn –version  #查看是否安装正确
   ```

9. #### nexus

   ```
   #yum install ksh  #安装ksh
   1）	在root用户创建nexus组和nexus用户
   2）	[root@RHL7 ~]# groupadd nexus
   3）	[root@RHL7 ~]# useradd –s /bin/ksh -d /app/nexus -g nexus nexus
   4）	[root@RHL7 ~]# passwd nexus
   进入nexus用户，su  – nexus 
   将nexus-2.14.3-02-bundle.tar.gz包上传到/app/nexus目录下，并解压
   [nexus@RHL7 ~]$ tar -zxvf  nexus-2.14.3-02-bundle.tar.gz
   上传jdk-7u80-linux-x64.tar.gz到/app/nexus下解压
   到nexus安装目录下   cd bin/jsw/conf/
   vim wrapper.conf
   修改nexus的jdk版本
   改成1.7的jdk版本
   ./nexus start  #在bin目录下执行   开启nexus
   登陆http://ip:8081/nexus/index.html。进入私服仓库。并登录
    默认用户/密码：admin/admin123、deployment/deployment123。
   将maven_pro 传到/app/nexus/sonatype-work/nexus/storage/storagev8（无则创建）
   $ mv maven_pro/ maven_pro_sit/
   $ chgrp nexus /app/nexus/sonatype-work/nexus/storage/storagev8/maven_pro_sit/*
   $ chown nexus /app/nexus/sonatype-work/nexus/storage/storagev8/maven_pro_sit/*
   $ cd maven_pro_sit/
   $ mv * ../
   
   ```

   配置nexus

   ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161431.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161454.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161509.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161519.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161535.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161544.jpg)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161600.png)

   ![img](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161618.jpg)

1. #### Eureka&Redis-v8core用户

   在root用户创建v8core组和v8core用户

   [root@RHL7 ~]# groupadd v8core 

   [root@RHL7 ~]# useradd -s /bin/ksh -d /app/v8core -g v8core v8core

   [root@RHL7 ~]# passwd v8core

   v8core用户下

   上传v8-eureka-rh7.5.tar.gz，v8-redis-rh7.5.tar.gz到目录下解压，进入/app/v8core/bin目录下

   启动Eureka

   ./eurStart

   解压v8-redis-rh7.5.tar.gz到目录下，配置redis/conf/redis.conf

   dir /app/v8core/redis/data

   修改bin/redisStart

   $HOME/redis/bin/redis-server $HOME/redis/conf/redis.conf

   修改bin/redisStop

   $HOME/redis/bin/redis-cli shutdown

1. #### EDSP-LOADER-v8core用户

   创建数据库用户loaderdb和数据库loaderdb 导入数据库sql文件

   ```
   create database loaderdb;
   create` `user` `'loaderdb'``@``'%'` `identified ``by` `'loaderdb123';
   grant` `all` `privileges` `on` `loaderdb.* ``to 'loaderdb'``@``'%';
   flush privileges; 
   ```
   
   
   v8core目录下上传edsp-loader-dist-3.0.15-RELEASE.tar.gz并解压重命名为edsp-loader
   
   配置loader配置文件config/application.yml中为test级别
   
   配置config/application-test.yml

   ```
   #指定启动端口
   server:
     port: 8075
     
   ###### 数据库方言 二选一 #####
   dialect: cn.sunline.edsp.tsp.dao.plugins.mybatis.dialect.MySQLDialect
   #dialect:    cn.sunline.edsp.tsp.dao.plugins.mybatis.dialect.OracleDialect
       ###### 数据库方言 二选一 #####
   
       defaultDataSourceId: datasource0
       dataSourceModel[0]:
        id: datasource0
        username: loaderdb
        password: loaderdb
        initialSize: 10
        minIdle: 5
        maxActive: 50
        ##### mysql开始 ####
        url: jdbc:mysql://192.168.19.138:3306/loaderdb?useUnicode\=true&characterEncoding\=utf-8
        ##### mysql结束 ####
   
        ##### oracle开始 ####
        #driverClassName: oracle.jdbc.driver.OracleDriver
        #url: jdbc:oracle:thin:@10.22.0.141:1521:HXDB
        #validationQuery: SELECT * FROM dual
        ##### oracle结束 ####
       #============== druid 配置==============
       druid:
        deny: 192.168.19.2
        allow: 127.0.0.1,192.192.168.19.138
        loginUsername: v8core
        loginPassword: v8core
   ```

   loader目录下bin/startup.sh启动登录，http://ip:8075  账密 admin sunline

   拖动ui包至页面新增模块

1. #### EDSP-CD -root用户

   上传edsp-cd-2.5.7-RELEASE-FULL.tar.gz至/opt目录下解压

   进入目录执行 sh ./install_cd.sh输入ip

   cd /opt/edsp-assistant/edsp-cd/etc

   vi cd-server.conf

   ```
   edsp_server_host: '192.168.19.138'
   
   edsp_server_url: 'http://192.168.19.138:5000'
   
   edsp_auth_api: 'http://192.168.19.138:8075/loader/api/uiAuth/user-info'
   
   edsp_download_server_url: 'http://192.168.19.139:5010'
   
   ### MongoDB single node mode
   mongodb_host: 127.0.0.1
   mongodb_port: 27017
   mongodb_db: edsp_dgyh
   
   ### MongoDB Auth
   # mongodb_db: edsp_cd
   # mongodb_user:
   # mongodb_password: '<mongodb password>'
   
   salt_api_password: '*c829bf0f6bc8e2927fd72362d0c5cd0cdfdf833b1e2d78194e9bf7573bfc5ccc'
   ```

   解压/opt/edsp-assistant/20200403edsp_dgyh

   确认edsp-mongod服务启动

   执行/opt/edsp-assistant/mongodb/bin/mongorestore -d edsp_dgyh --dir  /opt/edsp-assistant/20200403edsp_dgyh/edsp_dgyh
   
   重启服务
   systemctl restart edsp-mongod
   systemctl  restart edsp-redis
   systemctl restart salt-master
   systemctl restart salt-api
   systemctl restart cd-server
   systemctl restart cd-worker
   systemctl restart cd-beat

1. #### nginx

   在/usr/local/src下解压nginx-1.14.0.tar.gz

   cd /usr/local/src/nginx-1.14.0依次执行

   ```
   yum -y install gcc gcc-c++ autoconf pcre pcre-devel make automake
   yum -y install wget httpd-tools vim
   ./configure --prefix=/usr/local/nginx  --with-http_dav_module --with-http_stub_status_module --with-http_addition_module --with-http_sub_module --with-http_flv_module  --with-http_mp4_module  //执行此步需要有gcc   没有先下载	
   make && make install   
   groupadd nginx
   useradd -s /bin/bash -d /nginx -g nginx nginx
   passwd nginx
   chown –R nginx:root /usr/local/nginx
   ```

   配置nginx.conf

   ```
   user  root;
   worker_processes  1;
   
   #error_log  logs/error.log;
   #error_log  logs/error.log  notice;
   #error_log  logs/error.log  info;
   
   #pid        logs/nginx.pid;
   
   
   events {
       worker_connections  1024;
   }
   
   
   http {
       include       mime.types;
       default_type  application/octet-stream;
   
       #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
       #                  '$status $body_bytes_sent "$http_referer" '
       #                  '"$http_user_agent" "$http_x_forwarded_for"';
   
       #access_log  logs/access.log  main;
   
       sendfile        on;
       #tcp_nopush     on;
   
       #keepalive_timeout  0;
       keepalive_timeout  65;
   
       #gzip  on;
       server {
           listen       5010;
           server_name  localhost;
           root         /opt/edsp-assistant/edsp-cd/files/;
   
           #charset koi8-r;
   
           #access_log  logs/host.access.log  main;
   
           location / {
               autoindex on;
               #index  index.html index.htm;
               autoindex_exact_size on;
           }
   
           #error_page  404              /404.html;
   
           # redirect server error pages to the static page /50x.html
           #
           error_page   500 502 503 504  /50x.html;
           location = /50x.html {
               root   html;
           }
   
           # proxy the PHP scripts to Apache listening on 127.0.0.1:80
           #
           #location ~ \.php$ {
           #    proxy_pass   http://127.0.0.1;
           #}
   
           # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
           #
           #location ~ \.php$ {
           #    root           html;
           #    fastcgi_pass   127.0.0.1:9000;
           #    fastcgi_index  index.php;
           #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
           #    include        fastcgi_params;
           #}
   
           # deny access to .htaccess files, if Apache's document root
           # concurs with nginx's one
           #
           #location ~ /\.ht {
           #    deny  all;
           #}
       }
       # another virtual host using mix of IP-, name-, and port-based configuration
       #
       #server {
       #    listen       8000;
       #    listen       somename:8080;
       #    server_name  somename  alias  another.alias;
   
       #    location / {
       #        root   html;
       #        index  index.html index.htm;
       #    }
       #}
   
   
       # HTTPS server
       #
       #server {
       #    listen       443 ssl;
       #    server_name  localhost;
   
       #    ssl_certificate      cert.pem;
       #    ssl_certificate_key  cert.key;
   
       #    ssl_session_cache    shared:SSL:1m;
       #    ssl_session_timeout  5m;
   
       #    ssl_ciphers  HIGH:!aNULL:!MD5;
       #    ssl_prefer_server_ciphers  on;
   
       #    location / {
       #        root   html;
       #        index  index.html index.htm;
       #    }
       #}
   
   }
   ```
   
   /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
   
14. #### GATEWAY-GOVERNOR

    上传zhenxgw.tar.gz至/opt/edsp-gateway/（无则创建）下解压

    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161754.png)

    并重命名为以上两个文件夹

    配置governor下的etc/governor/application.yml

    ```
    #----- 暴露端口 start! ---------------------------------------------------------------
    server:
      port: 9098
    #----- 暴露端口 end! -----------------------------------------------------------------
    
    #----- mysql数据源配置，只需要修改数据库的url，username和password start ---------------
      datasource:
        driver-class-name: com.mysql.jdbc.Driver
        url: ${MYSQL_URL:jdbc:mysql://192.168.19.138:3306/gwbasedb?characterEncoding=utf-8&allowMultiQueries=true}
        username: ${MYSQL_USERNAME:root}               # 用户名
        password: ${MYSQL_PASSWORD:root}               # 数据密码加密配置:DES(0833F4DB2EDDB3E6)
        initialization-mode: always                    # 初始化模式，总是执行
    
      #----- redis配置 start --------------------------------------------------------
      redis:
        enabled: ${REDIS_ENABLED:false}
        information: redis://192.168.19.138:6379?password=abcd&pool_min=10&pool_max=250&pool_maxwaitmillis=3000&syncInterval=5000
        type: ${REDIS_TYPE:cluster}                     # sentinel: 哨兵模式, cluster: 集群模式, 不配置默认单机
        sentinelMaster: ${REDIS_SENTINEL_MASTER:mymaster}
        timeout: 1000
        maxTotal: 1000
        maxIdle:  50
        minIdle:  10
        encryptEnable: ${REDIS_ENCRYPT_ENABLE:false}     # 数据库密码是否需要解密,true时密码不能为空，格式为DES(EAB4F04FAC628F82)
        password: ${REDIS_PASSWORD:}                     # 选填
        database: 0                                      # 选填，默认为0
        nodes: ${REDIS_NODES:10.22.4.240:7006}
      #----- redis配置 end ！ --------------------------------------------------------
    
    #----- eureka配置，默认不开启，如需使用rest负载模式需开启 start ---------------------------------
    eureka:
      instance:
        hostname: ${SERVER_HOSTNAME:192.168.19.138}
        prefer-ip-address: true
        ip-address: ${SERVER_ADDRESS:192.168.19.138}
      client:
        enabled: ${EUREKA_ENABLED:true}                        # 是否开启eureka注册，不能和zookeeper的enabled不可同时为true
        registerWithEureka: true
        fetchRegistry: true
        serviceUrl:
          defaultZone: ${EUREKA_ADDRESS:http://192.168.19.138:8096/eureka/v2/} # eureka服务器地址
    #----- eureka配置，默认不开启，如需使用rest负载模式需开启 end ------------------
    ```

    bin目录下 ./gateway start governor启动

15. #### GATEWAY-SERVER

    配置etc/server/application.yml

    ```
    #-------接入配置(1)：HTTP REST接入配置 start ! ------------------------------------------------------
    server:
      port: 9013                                                         # rest接入暴露端口
      tomcat:
        max-threads: 400                                                 # 最大线程池
        max-connections: 3000
        accept-count: 200          
        
    #------ 网关内部配置 start！-----------------------------------------------------------------------------
    gateway:
      server:
        registerAddress: ${REGISTER_ADDRESS:http://192.168.19.138:9098}  # 网关治理服务的地址
        registerToken: ${REGISTER_TOKEN:EcdE/jcXR8mPD+phJUMqMjvNtPzawDX+KLtK9cSPGt8=}                           # 网关治理服务对应注册token,在管控端的全局配置页面获取
        instanceIp: ${INSTANCE_IP:}                                 # 适配多网卡，手动配置IP
        instancePort: ${INSTANCE_PORT:}                             # 适配docker容器中端口映射的情况
        instanceName: ${INSTANCE_NAME:}                             # 实例名，默认是IP:PORT
        httpCodeEnable: true                                        # 是否返回HTTP原本的状态码
        httpUrlPattern: ${HTTP_URL_PATTERN:/gateway/*}              # 网关拦截HTTP请求的路径
    
    #------ eureka配置，默认不开启，如需使用rest负载模式需开启 start ------------------------------------
    eureka:
      instance:
        hostname: ${SERVER_HOSTNAME:192.168.19.138}
        prefer-ip-address: true
        ip-address: ${SERVER_ADDRESS:192.168.19.138}
      client:
        enabled: ${EUREKA_ENABLED:true}
        registerWithEureka: true
        fetchRegistry: true
        serviceUrl:
          defaultZone: ${EUREKA_ADDRESS:http://192.168.19.138:8096/eureka/v2/}
    #----- eureka配置，默认不开启，如需使用rest负载模式需开启 end ---------------------------------------
    
    ```

    bin目录下./gateway start server启动

16. #### DRS-SERVER

    上传zhenxdrs.tar.gz到/opt/edsp-drs/（无则创建）下解压

    分别找到解压的server，governor重命名

    ![image-20210302161847913](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161847.png)

    配置etc/server/application.yml

    ```
    server:
      port: 8088
    eureka:
      instance:
        prefer-ip-address: true
        ip-address: 192.168.19.138
        appname: drs-server
      client:
        register-with-eureka: true
        fetch-registry: true
        serviceUrl:
          defaultZone: http://192.168.19.138:8096/eureka/v2/
    spring:
      application:
        name: drs-server
    ```

    配置etc/server/drs.properties

    ```
    ##rpcserver\u4F7F\u7528\u7684\u6CE8\u518C\u4E2D\u5FC3 \u4E0D\u652F\u6301\u591A\u6CE8\u518C\u4E2D\u5FC3
    registry.address=zookeeper://192.168.19.138:2182
    
    jdbc.username=root
    jdbc.password=root
    
    #\u591A\u4E2A\u5E93\u5219\u4F9D\u7167\u4E0B\u9762\u683C\u5F0F\u914D\u7F6E\uFF1Ajdbc.dataNode1\u3001jdbc.dataNode2\u2026\u2026\uFF08\uFF09
    #\u8FD9\u91CCURI\u6307\u5B9A\u4E86\u7528\u6237\u540D\u548C\u5BC6\u7801\uFF0C\u867D\u7136\u516C\u5171\u914D\u7F6E\u4E5F\u6709\u6307\u5B9A\uFF0C\u4F46\u4E0D\u4F1A\u751F\u6548\uFF08URI\u62FC\u63A5\u7684\u53C2\u6570\uFF0C\u5E94\u8BE5\u53BB\u9664 jdbc.\uFF09
    #jdbc.dataNode1\u4E3A\u9ED8\u8BA4\u5E93\uFF0C\u7528\u4E8E\u5B58\u653E\u4E0D\u5206\u5E93\u5206\u8868\u7684\u914D\u7F6E\u6570\u636E
    #jdbc.dataNode1=jdbc:mysql://10.22.0.131:30306/drs5.4
    jdbc.dataNode0=jdbc:mysql://192.168.19.138:3306/drsbasedb
    
    #redis.master1=10.22.0.131:32382
    redis.cluster.enable=false
    redis.master1=192.168.19.138:6379?maxIdle=50&connectionTimeout=3000&maxWaitMillis=3000&password=&minIdle=0&testOnBorrow=true&testWhileIdle=true
    #rpc\u8981\u7D20
    serviceId=drs-server
    #group=CS
    #version=1.0
    
    #protocolConfig
    protocol.name=edsp
    protocol.hostIp=192.168.19.138
    protocol.port=18289
    
    protocol.trace.enable=true
    protocol.trace.address=http:192.168.19.138:9411/api/v1/spans
    protocol.trace.pushMetrics=true
    protocol.trace.writeMetrics=true
    
    protocol.threadPool=fixed
    protocol.maxThread=100
    protocol.minThread=100
    protocol.queues=0
    ```

    bin目录下./drs start server

17. #### DRS-GOVERNOR

    配置etc/governor/application.yml,	其中redis.port与redis.host同级

    ```
    #####暴露端口######
    server:
      port: 7999
    dataWarmUp:
      limitPer: 900
    #####应用名######
    spring:
      application:
        name: drs
      redis:
        host: 192.168.19.138
        port: 6379
    ```

    配置etc/governor/drs.properties

    ```
    jdbc.username=root
    jdbc.password=root
    
    #\u591A\u4E2A\u5E93\u5219\u4F9D\u7167\u4E0B\u9762\u683C\u5F0F\u914D\u7F6E\uFF1Ajdbc.dataNode0\u3001jdbc.dataNode1\u2026\u2026\uFF08\uFF09
    #\u8FD9\u91CCURI\u6307\u5B9A\u4E86\u7528\u6237\u540D\u548C\u5BC6\u7801\uFF0C\u867D\u7136\u516C\u5171\u914D\u7F6E\u4E5F\u6709\u6307\u5B9A\uFF0C\u4F46\u4E0D\u4F1A\u751F\u6548\uFF08URI\u62FC\u63A5\u7684\u53C2\u6570\uFF0C\u5E94\u8BE5\u53BB\u9664 jdbc.\uFF09
    #jdbc.dataNode0\u4E3A\u9ED8\u8BA4\u5E93\uFF0C\u7528\u4E8E\u5B58\u653E\u4E0D\u5206\u5E93\u5206\u8868\u7684\u914D\u7F6E\u6570\u636E
    #jdbc.dataNode1=jdbc:mysql://10.22.0.131:30306/drs5.4
    #jdbc.dataNode0=jdbc:oracle:thin:@10.10.20.40:1521:HXDB
    jdbc.dataNode0=jdbc:mysql://192.168.19.138:3306/drsbasedb
    
    ##redis\u8282\u70B91 , \u652F\u6301url\u53C2\u6570\u62FC\u63A5
    redis.master1=192.168.19.138:6379?maxIdle=50&connectionTimeout=3000&maxWaitMillis=&password=&minIdle=0&testOnBorrow=true&testWhileIdle=true
    ##master1\u5BF9\u5E94\u7684\u4ECE\u8282\u70B9\uFF0C\u591A\u4E2A\u7528\uFF0C\u53F7\u9694\u5F00\uFF0C\u4E14\u652F\u6301url\u53C2\u6570\u62FC\u63A5
    ##\u4E3B\u4ECE\u6A21\u5F0F\u4E0B\uFF0C\u8BFB\u5199\u5206\u79BB\uFF0C\u5C06\u4F18\u5148\u8BFB\u53D6\u4ECE\u8282\u70B9\u4E2D\u7684\u6570\u636E
    #redis.slave1=10.22.0.131:30379?maxIdle=150,10.22.0.131:30379
    
    ```

    bin目录下./drs start governor

18. #### Bat

    上传edsp-bat-governor-tran-4.1.0-M11-1582706577899.tar.gz到/opt/edsp-bat/（无则创建）下解压并重命名为bat-governor

    配置etc/governor/application.properties

    ```
    #数据源配置MYSLQL
    adp.datasource.enabled=true
    adp.datasource.defaultDataSourceId=FRWDB
    adp.datasource.defaultNewDataSourceId=FRWDB
    adp.datasource.druid.enabled=true
    adp.datasource.druid.dataSourceModel[0].id=FRWDB
    adp.datasource.druid.dataSourceModel[0].url=jdbc:mysql://192.168.19.138:3306/batbasedb?characterEncoding=utf-8&serverTimezone=GMT%2B8&connectTimeout=60000&socketTimeout=60000
    adp.datasource.druid.dataSourceModel[0].username=root
    adp.datasource.druid.dataSourceModel[0].password=root
    adp.datasource.druid.dataSourceModel[0].initialSize=20
    adp.datasource.druid.dataSourceModel[0].minIdle=20
    adp.datasource.druid.dataSourceModel[0].maxActive=100
    adp.datasource.druid.dataSourceModel[0].maxWait=60000
    adp.datasource.druid.dataSourceModel[0].timeBetweenEvictionRunsMillis=60000
    adp.datasource.druid.dataSourceModel[0].minEvictableIdleTimeMillis=300000
    adp.datasource.druid.dataSourceModel[0].validationQuery=select 1 from dual
    
    #联机服务[0].id
    
    #协议地址
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.address=http://192.168.19.138:28007
    #最小线程数
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.minThread=60
    #最大线程数
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.maxThread=60
    #线程池队列数
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.queuesSize=100
    #是否长连接
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.lengConnection=true
    #线程空闲保持时间
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.threadKeepAliveTime=0
    #连接超时时间
    adp.online.server.onlineServerConfigs[0].onlineAccessProtocolConfig.connectTimeout=0
    #服务注册中心配置.主机地址
    adp.online.server.onlineServerConfigs[0].onlineServiceRegistryConfig.host=192.168.19.138
    
    #远程服务调用.RPC协议配置
    
    #注册中心地址
    adp.remote.service.remoteServiceConfigs[0].registryHost=192.168.19.138
    #注册中心端口
    adp.remote.service.remoteServiceConfigs[0].registryPort=8096
    #远程服务注册分组
    adp.remote.service.remoteServiceConfigs[0].registryGroup=edsp
    #报文传输类型
    adp.remote.service.remoteServiceConfigs[0].pkgTransferType=string
    
    #如果启动drs路由,服务提供者的应用名格式为 应用名+"."+dcn编号，如下所示，注意英文字母小写
    spring.application.name=09001
    
    edsp-drsServer=drs-server
    
    eureka.instance.hostname=${EUREKA_CLIENT_IP_ADDRESS:192.168.19.138}
    eureka.instance.prefer-ip-address=true
    eureka.instance.ip-address=${EUREKA_CLIENT_IP_ADDRESS:192.168.19.138}
    eureka.client.registerWithEureka=true
    eureka.client.fetchRegistry=true
    eureka.client.serviceUrl.defaultZone=${EUREKA_CLIENT_URL:http://192.168.19.138:8096/eureka/v2/}
    ```

    bin目录下./bat start governor

19. #### Git

    ```
    yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel
    yum -y install git-core
    git --version  #查看git版本
    ```

20. #### GitLab

    ```
    sudo yum install -y curl policycoreutils-python openssh-server
    sudo systemctl enable sshd
    sudo systemctl start sshd
    sudo firewall-cmd --permanent --add-service=http
    sudo systemctl reload firewalld
    sudo yum install postfix
    sudo systemctl enable postfix
    sudo systemctl start postfix
    rpm -i gitlab-ce-10.7.5-ce.0.el7.x86_64.rpm     
    vi /etc/gitlab/gitlab.rb
     
    gitlab-ctl reconfigure   #初始化gitlab
     gitlab-ctl start   #启动gitlab
     登录gitlab
    在浏览器中输入http://localhost 或者直接IP地址http://10.10.60.47（我的ip是：10.10.60.47）------登录地址是上面设置的url或者默认的url
     第一次访问，会提示提示输入密码，此时的用户名是root。密码设置 完成后，自动跳转到登录界面
    ```

    

21. #### GitLabRunner

    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161936.png)

    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302161950.png)

    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162110.png)
    
    ```
    rpm -ivh gitlab-ci-multi-runner-9.5.1-1.x86_64.rpm
       systemctl daemon-reload
    systemctl enable gitlab-runner
    systemctl restart gitlab-runner
    gitlab-runner register
    
    上面两张图中红色框部分按照gitlab中的值填写，此处为全局的地址和token，也可以使用某个project的地址和token。
    注册成功后，刷新gitlab页面会显示runner节点。
    vi /etc/gitlab-runner/config.toml
    concurrent = 1
    check_interval = 0
    
    [[runners]]
    name = "devops"
    url = "http://10.10.20.103/"
    token = "f5b0ea353ef2f112b0d2400f6ed8ea"
    executor = "shell"
    output_limit = 40960
    limit = 0
    [runners.cache]
    修改PIPELINE文件传输大小限制
    登录gitlab修改

    ```

    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162031.png)
    
    ![](https://gitee.com/oxrial/cloud-note_-pic-go/raw/master/image/20210302162045.png)

