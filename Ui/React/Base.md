# 关于事件
例：onClick事件的形参event是经过React包装的对象
## 禁用默认行为与事件冒泡（当div内部含有元素的点击事件被触发，由于是div的子集，也会默认调用div的点击事件）
例：a标签的href跳转，若加上onClick属性事件，可以在事件中拦截
```javascriptxml
<div onClick={() => {console.log(1)}}>
    <a href="http://xxxx.com" onClick={(e) => {
        // 禁用默认行为
        e.preventDefault()
        // 禁用事件冒泡行为
        e.stopPropagation()
    }} />
</div>
```