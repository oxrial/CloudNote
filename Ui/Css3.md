### 阴影

**box-shadow**：x y b color;

x:横向偏移，y:纵向偏移，b:模糊，color:颜色

可重复叠加效果例box-shadow：x y b color，x y b color，x y b color;



### 2D转换

**transform**：转换可以对元素进行移动、缩放、转动、拉长或拉伸。

**transform-origin**：在上述属性**配置后**可对元素进行进一步的变更元素位置



### 动画过渡

**transition**：*property duration timing-function delay*;

​	*duration*：transition效果需要指定多少秒或毫秒才能完成    (3s)

​	*timing-function*： 指定transition效果的转速曲线                  (linear|ease|ease-in|ease-out|ease-in-out|cubic-bezier(*n*,*n*,*n*,*n*))

​	*delay*： 定义transition效果开始的时候等待耗时                     (0)



### 多列

**columns**：width count;

​	*column-count：指定需要分割的列数。*number*;

​	*column-width*：列宽

***column-gap***：列列间隙。px

**column-rule**：width style color

​	*column-rule-style*：列边框

​	*column-rule-width*：列边框宽

​	*column-rule-color*：列边框色



### 盒子模型

**box-sizing**：重定义某区域的内容，告诉浏览器该使用哪种盒模型来渲染文档。默认是*content-box* 

​		文本盒子、	边框盒子、继承父盒子

（content-box|border-box|inherit:）

content-box：指定盒模型为 W3C 标准模型，设置 border、padding 会增加元素 width与 height 的尺寸，即 border 与 padding 相当于是元素的“殖民地”，元素的“土地”、尺寸会增加，为向外延伸。

border-box：指定盒模型为 IE模型（怪异模式），设置 border、padding 不会影响元素 width 与 height 的尺寸，即 border 与 padding 由元素已设空间转变。即空间还是这个空间，只是将部分空余的地方，转变成了其他空间用法而已，为内部转变。



### 外形修饰 （不占空间）

**outline**：width style color;（参照border）

**outline-offset**：偏移量

