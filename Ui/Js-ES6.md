<!-- vscode-markdown-toc -->

- 1. [Js](#Js)
  - 1.1. [var、let、const](#varletconst)
  - 1.2. [解构](#)
    - 1.2.1. [不得使用圆括号的解构赋值](#-1)
    - 1.2.2. [可以使用圆括号的结构赋值](#-1)
    - 1.2.3. [用途](#-1)
    - 1.2.4. [操纵数组](#-1)
    - 1.2.5. [同步 foreach](#foreach)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->##  1. <a name='Js'></a>Js

---

### 1.1. <a name='varletconst'></a>var、let、const

var 存在变量提升 console.log()会 underfined

let、const 不存在变量提升 console.log()会 is not defined

let 和 const 不能声明**同名变量**，var 可以

var 声明的变量会挂载至 window，let、const 不会 window.a

在一个块内 let**声明变量前**若有属性赋值，后若 let**再声明**该属性则声明前报错（**暂时性死区**）

let 声明的变量只在 let 命令所在的**代码块**内有效

const 声明一个**只读的变量**，声明必须赋值不可 null，声明后值不能改变，**复合类型数据**可以变更属性

### 1.2. <a name=''></a>解构

如果要将一个已经声明的变量用于解构赋值，必须非常小心。

```javascript
// 错误的写法
let x;
{x} = {x: 1};
// SyntaxError: syntax error
```

上面代码的写法会报错，因为 JavaScript 引擎会将`{x}`理解成一个代码块，从而发生语法错误。只有不将大括号写在行首，避免 JavaScript 将其解释为代码块，才能解决这个问题。

```javascript
// 正确的写法
let x
;({ x } = { x: 1 })
```

上面代码将整个解构赋值语句，放在一个圆括号里面，就可以正确执行。关于圆括号与解构赋值的关系。

#### 1.2.1. <a name='-1'></a>不得使用圆括号的解构赋值

（1）变量声明语句

```javascript
// 全部报错
let [(a)] = [1];

let {x: (c)} = {};
let ({x: c}) = {};
let {(x: c)} = {};
let {(x): c} = {};

let { o: ({ p: p }) } = { o: { p: 2 } };
```

上面 6 个语句都会报错，因为它们都是变量声明语句，模式不能使用圆括号。

（2）函数参数

函数参数也属于变量声明，因此不能带有圆括号。

```javascript
// 报错
function f([(z)]) { return z; }
// 报错
function f([z,(x)]) { return x; }
```

（3）赋值语句的模式

```javascript
// 全部报错
({ p: a }) = { p: 42 };
([a]) = [5];
```

上面代码将整个模式放在圆括号之中，导致报错。

```javascript
// 报错
[({ p: a }), { x: c }] = [{}, {}];
```

上面代码将一部分模式放在圆括号之中，导致报错。

#### 1.2.2. <a name='-1'></a>可以使用圆括号的结构赋值

赋值语句的非模式部分，可以使用圆括号。

```javascript
;[b] = [3] // 正确
;({ p: d } = {}) // 正确
;[parseInt.prop] = [3] // 正确
```

上面三行语句都可以正确执行，因为首先它们都是赋值语句，而不是声明语句；其次它们的圆括号都不属于模式的一部分。第一行语句中，模式是取数组的第一个成员，跟圆括号无关；第二行语句中，模式是`p`，而不是`d`；第三行语句与第一行语句的性质一致。

#### 1.2.3. <a name='-1'></a>用途

变量的解构赋值用途很多。

**（1）交换变量的值**

```javascript
let x = 1
let y = 2

;[x, y] = [y, x]
```

上面代码交换变量`x`和`y`的值，这样的写法不仅简洁，而且易读，语义非常清晰。

**（2）从函数返回多个值**

函数只能返回一个值，如果要返回多个值，只能将它们放在数组或对象里返回。有了解构赋值，取出这些值就非常方便。

```javascript
// 返回一个数组

function example() {
	return [1, 2, 3]
}
let [a, b, c] = example()

// 返回一个对象

function example() {
	return {
		foo: 1,
		bar: 2
	}
}
let { foo, bar } = example()
```

**（3）函数参数的定义**

解构赋值可以方便地将一组参数与变量名对应起来。

```javascript
// 参数是一组有次序的值
function f([x, y, z]) { ... }
f([1, 2, 3]);

// 参数是一组无次序的值
function f({x, y, z}) { ... }
f({z: 3, y: 2, x: 1});
```

**（4）提取 JSON 数据**

解构赋值对提取 JSON 对象中的数据，尤其有用。

```javascript
let jsonData = {
	id: 42,
	status: 'OK',
	data: [867, 5309]
}

let { id, status, data: number } = jsonData

console.log(id, status, number)
// 42, "OK", [867, 5309]
```

上面代码可以快速提取 JSON 数据的值。

**（5）函数参数的默认值**

```javascript
jQuery.ajax = function (
	url,
	{
		async = true,
		beforeSend = function () {},
		cache = true,
		complete = function () {},
		crossDomain = false,
		global = true
		// ... more config
	} = {}
) {
	// ... do stuff
}
```

指定参数的默认值，就避免了在函数体内部再写`var foo = config.foo || 'default foo';`这样的语句。

**（6）遍历 Map 结构**

任何部署了 Iterator 接口的对象，都可以用`for...of`循环遍历。Map 结构原生支持 Iterator 接口，配合变量的解构赋值，获取键名和键值就非常方便。

```javascript
const map = new Map()
map.set('first', 'hello')
map.set('second', 'world')

for (let [key, value] of map) {
	console.log(key + ' is ' + value)
}
// first is hello
// second is world
```

如果只想获取键名，或者只想获取键值，可以写成下面这样。

```javascript
// 获取键名
for (let [key] of map) {
	// ...
}

// 获取键值
for (let [, value] of map) {
	// ...
}
```

**（7）输入模块的指定方法**

加载模块时，往往需要指定输入哪些方法。解构赋值使得输入语句非常清晰。

```javascript
const { SourceMapConsumer, SourceNode } = require('source-map')
```

#### 1.2.4. <a name='-1'></a>操纵数组

```
var arr = [1,2,3,4]
```

##### 返回一个新数组

slice （起始位置 start，终止位置 end【不包含 end】）从原数组中选取指定区域的值

```
arr.slice(1,2)  => // [2]
```

##### 原数组的直接变更操作

splice （起始位置 index，删除数,length，...新增数组元素项）

```
arr.splice(1,1,'3') => [1,'3',3,4]
```

shift （数组的第一个元素从其中删除，并返回第一个元素的值）

```
console.log(arr.shift()); //1    去除 且 返回去除的元素
console.log(arr); //[2,3,4]
```

unshift （向数组的开头添加一个或更多元素，并返回新的长度）

##### 逆序

reverse

```
arr.reverse()   // [4,3,2,1]
```

##### 排序

```
sort()   字母表排序
sort(function) 根据方法中的return规则排序
sort(sortBy('x',true)) 根据参数值升降序
```

##### 数组元素拼接，返回一个字符串

```
var a = [1,2,3,4]
var b = a.join("*")
console.log(b)    // "1*2*3*4"
```

#### 1.2.5. <a name='foreach'></a>同步 foreach

```
async function asyncForEach(arr, callback) {
	for (let i=0;i<arr.length;i++) {
		await callback(arr[i],i,arr)
	}
}
```

```
new Promise(async () => {
	asyncForEach(arr,asunc () => {
		await function()
	})
})
```
