#### times: fori循环

```
//js原生的循环方法
for(var i = 0; i < 5; i++){
	console.log(i);
}

console.log('------- lodash -------');

//ladash的times方法
_.times(5,function(a){
	console.log(a);
});
```

