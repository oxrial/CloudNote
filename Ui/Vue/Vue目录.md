项目结构

---

├─dist
│              
├─node_modules
│                
├─public #静态资源
│      favicon.ico #favicon图标
│      index.html #html模板
│      
├─src #源代码
│   │  
│   ├─assets #需打包的图片资源
│   │  ├─js
│   │  │      comm.js
│   │  │      indexjk.js
│   │  │      
│   │  └─scss
│   │          _variable.scss
│   │          
│   ├─components #全局公用组件
│   │          
│   ├─router #路由
│   │      index.js
│   │      
│   ├─store #全局store管理
│   │      index.js
│   │      
│   ├─views #所有页面
│   │
│   │  App.vue #入口页面
│   │  main.js #入口文件 加载组件 初始化
│  .browserslistrc
│  .eslintrc.js
│  .gitignore
│  atlantis_ui.7z
│  babel.config.js #babel配置
│  debug.log
│  package-lock.json
│  package.json #架包配置
│  README.md
│  tree.txt
│  vue.config.js #vue-cli配置

