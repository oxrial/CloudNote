1. 构建的vue/cli@4项目

2. 构建vite-vue3项目
3. 复制vuecli项目的package.json中的依赖到vite项目的package.json中，去除vue/cli依赖

```
dependencies:{}
devDependencies:{}
```

​	npm install

4. 添加@路径

​	vite.config.js文件中
```
import * as path from 'path'
```

​	添加
```
defineConfig({
	...
 	resolve: {
  		alias: {
   			'@': path.resolve('src')
  		}
 	}
})
```

​	npm run dev

**注：**Err：Uncaught ReferenceError: process is not defined

5. 修改router目录下的index.js中createRouter项，Vite 不识别 `process.env`, 取而代之的是：`import.meta.env.`





